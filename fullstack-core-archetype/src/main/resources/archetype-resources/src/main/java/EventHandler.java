#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

public interface EventHandler<E extends Event>  {
  void on(E event);
}
