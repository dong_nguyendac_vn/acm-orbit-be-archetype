#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto.request;

import ${package}.DataTransferObject;

public interface Request extends DataTransferObject {

}
