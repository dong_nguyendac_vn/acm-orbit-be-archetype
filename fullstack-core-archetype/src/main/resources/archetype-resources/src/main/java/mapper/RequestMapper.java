#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mapper;

import ${package}.Command;
import ${package}.dto.request.Request;

public interface RequestMapper<R extends Request, C extends Command<?>> {

  public C transform(String identifier, R request);
}
