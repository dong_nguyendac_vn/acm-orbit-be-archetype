#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.exception.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum InternalServerMessage implements ApplicationMessage {
  INTERNAL_SERVER_ERROR("I10001", "Internal server error");

  private String message;
  private String description;

  @Override
  public String getKey() {
    return this.name();
  }


  @Override
  public int getStatusCode() {
    return 500;
  }
}
