#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mapper;

import ${package}.Command;
import ${package}.IdentifiableDomainObject;

public interface CommandMapper<L extends IdentifiableDomainObject<?>, C extends Command<?>> {

  L transform(String identifier, C command);
}
