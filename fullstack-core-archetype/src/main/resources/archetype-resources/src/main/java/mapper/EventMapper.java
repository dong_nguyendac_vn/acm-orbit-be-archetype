#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mapper;

import ${package}.Event;
import ${package}.IdentifiableDomainObject;

public interface EventMapper<E extends Event<?>, L extends IdentifiableDomainObject<?>> {

  public L transform(E entity);
}
