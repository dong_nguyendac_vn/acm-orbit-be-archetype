#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

public interface ValueObject extends DomainObject {

  boolean equals(Object obj);

  int hashCode();

}
