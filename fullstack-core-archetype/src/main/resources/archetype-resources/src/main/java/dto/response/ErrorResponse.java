#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@Getter
@Builder
@RequiredArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ErrorResponse implements Response {

  private static final long serialVersionUID = 4155190046875609566L;

  private final String error;
  private final String errorDescription;

  public Map<String, Object> toMap() {
    Map<String, Object> errorMap = new LinkedHashMap<>();
    errorMap.put("error", this.getError());
    errorMap.put("error_description", this.getErrorDescription());
    return errorMap;
  }

}
