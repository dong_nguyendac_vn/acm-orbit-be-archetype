#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.exception;

import ${package}.exception.message.ApplicationMessage;
import ${package}.exception.message.InternalServerMessage;
import javax.persistence.Transient;
import lombok.Getter;

@Getter
public class ApplicationException extends RuntimeException {

  private static final long serialVersionUID = 8133784150850998787L;

  @Transient
  private final transient ApplicationMessage error;

  public ApplicationException() {
    this.error = InternalServerMessage.INTERNAL_SERVER_ERROR;
  }

  public ApplicationException(String message) {
    super(message);
    this.error = InternalServerMessage.INTERNAL_SERVER_ERROR;
  }

  public ApplicationException(
      ApplicationMessage error) {
    super(error.getDescription());
    this.error = error;
  }

  public ApplicationException(Throwable cause, ApplicationMessage error) {
    super(error.getDescription(), cause);
    this.error = error;
  }

  public ApplicationException(String message, ApplicationMessage error) {
    super(message);
    this.error = error;
  }


}
