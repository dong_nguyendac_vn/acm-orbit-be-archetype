#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import java.io.Serializable;

public interface IdentifiableDomainObject<T extends Serializable> extends DomainObject {

  T getId();
}
