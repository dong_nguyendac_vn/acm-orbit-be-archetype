#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto.response;

import ${package}.DataTransferObject;

public interface Response extends DataTransferObject {

}
