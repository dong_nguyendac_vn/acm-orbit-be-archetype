#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.exception.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MethodNotAllowedMessage implements ApplicationMessage {
  METHOD_NOT_ALLOWED("M10001", "Method not allowed");

  private String message;
  private String description;

  @Override
  public String getKey() {
    return this.name();
  }

  @Override
  public int getStatusCode() {
    return 404;
  }
}
