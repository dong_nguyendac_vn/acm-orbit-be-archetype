#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto.response;


import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Getter
@Builder
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class IdResponse implements Response {

  private static final long serialVersionUID = 2146639230965132251L;

  private final String id;
}
