#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.exception.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ForbiddenMessage implements ApplicationMessage {
  FORBIDDEN("F10001", "Forbidden"),
  ;

  private String message;
  private String description;

  @Override
  public String getKey() {
    return this.name();
  }

  @Override
  public int getStatusCode() {
    return 403;
  }
}
