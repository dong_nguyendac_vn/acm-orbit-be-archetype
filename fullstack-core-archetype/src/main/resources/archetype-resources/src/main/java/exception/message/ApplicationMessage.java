#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.exception.message;

public interface ApplicationMessage {

  String getKey();

  String getMessage();

  String getDescription();

  default int getStatusCode() {
    return 200;
  }
}
