#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.mapper;

import ${package}.IdentifiableDomainObject;
import ${package}.dto.response.Response;

public interface ResponseMapper<R extends Response, D extends IdentifiableDomainObject<?>> {

  public R transform(D domain);
}
