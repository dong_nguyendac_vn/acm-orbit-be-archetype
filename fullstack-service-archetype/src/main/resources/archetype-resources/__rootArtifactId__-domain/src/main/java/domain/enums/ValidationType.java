#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ValidationType {
  NAME(TypeConstants.NAME);

  private final String name;

  @Override
  public String toString() {
    return name;
  }

  public interface TypeConstants {

    String NAME = "NAME";
  }
}
