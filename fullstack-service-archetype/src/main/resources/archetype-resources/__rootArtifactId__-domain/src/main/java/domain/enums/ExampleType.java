#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ExampleType {
  APARTMENT,
  LAND,
  VILLA,
  RETAIL,
  OFFICE,
  INDUSTRIAL,
  HOUSE,
  CONDO;

  @JsonValue
  public String getJsonValue() {
    return this.name().toLowerCase();
  }
}
