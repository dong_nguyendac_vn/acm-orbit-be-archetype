#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.dto.request;

import ${groupId}.core.dto.request.Request;
import ${package}.domain.enums.ExampleType;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode
@NoArgsConstructor(force = true)
@RequiredArgsConstructor
@Getter
@Builder
public class ExampleDTO implements Request {

  private static final long serialVersionUID = 8214810214010346123L;

  private final String name;
  private final ExampleType type;
  private final String description;

}
