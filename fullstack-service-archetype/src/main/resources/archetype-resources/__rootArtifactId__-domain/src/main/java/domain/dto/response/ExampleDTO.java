#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.dto.response;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import ${groupId}.core.valueobject.Timestamp;
import ${package}.domain.enums.ExampleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
@JsonPropertyOrder({"id", "name", "type", "description"})
public class ExampleDTO extends AuditableDTO {

  private static final long serialVersionUID = -6448790230392272127L;

  private final String id;
  private final String name;
  private final ExampleType type;
  private final String description;

  @Builder
  public ExampleDTO(String id,
      String name, ExampleType type, String description,
      Timestamp createdAt,
      Timestamp lastModifiedAt) {
    super(createdAt, lastModifiedAt);
    this.id = id;
    this.name = name;
    this.type = type;
    this.description = description;
  }
}
