#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.dto.response;

import ${groupId}.core.dto.response.Response;
import ${groupId}.core.valueobject.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Getter
public class AuditableDTO implements Response {

  private static final long serialVersionUID = -6448790230392272127L;

  protected Timestamp createdAt;

  protected Timestamp lastModifiedAt;


}
