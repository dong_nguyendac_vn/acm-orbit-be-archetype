#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {
  CREATED,
  VERIFIED;

  @JsonValue
  public String getJsonValue() {
    return this.name().toLowerCase();
  }
}
