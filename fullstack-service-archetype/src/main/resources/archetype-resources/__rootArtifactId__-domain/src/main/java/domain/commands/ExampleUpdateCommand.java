#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.domain.commands;

import ${groupId}.core.Command;
import ${package}.domain.enums.ExampleType;
import ${package}.domain.enums.Status;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
@ToString
@Getter
@Value
public class ExampleUpdateCommand implements Command<String> {

  private static final long serialVersionUID = 946970709267873394L;

  @EqualsAndHashCode.Include
  private final String identifier;

  private final String name;
  private final ExampleType type;
  private final String description;
  private final Status status;

}
