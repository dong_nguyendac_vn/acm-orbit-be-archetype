#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.spring;

import ${package}.service.error.ErrorHandler;
import ${package}.service.error.ErrorServiceLocator;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@Component
@RequiredArgsConstructor
public class ServiceErrorAttributes extends DefaultErrorAttributes {

  private final ErrorServiceLocator serviceFactory;
  @Qualifier("defaultErrorHandler")
  private final ErrorHandler defaultErrorHandler;

  @Override
  public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
    Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);

    Throwable throwable = getError(webRequest);
    log.error("Request with path {} got error",
        errorAttributes.getOrDefault("path", "not available"), throwable);
    return this.build(errorAttributes, throwable);

  }

  private Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable) {
    try {
      return serviceFactory.get(errorAttributes.get("status").toString())
          .build(errorAttributes, throwable);
    } catch (NoSuchBeanDefinitionException ex) {
      return defaultErrorHandler.build(errorAttributes, throwable);
    }
  }
}
