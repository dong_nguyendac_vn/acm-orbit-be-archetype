#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.service;

import ${package}.service.security.dto.SecurityPermission;
import ${package}.service.security.provider.CollectionSecurityPermissionClientProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

public abstract class AbstractCollectionJwtSecurityPermission<T extends SecurityPermission> extends
    AbstractJwtSecurityPermission<Collection<T>> implements
    JwtSecurityPermissionService {

  private final CollectionSecurityPermissionClientProvider<T> provider;

  public AbstractCollectionJwtSecurityPermission(Logger logger,
      CollectionSecurityPermissionClientProvider<T> provider) {
    super(logger);
    this.provider = provider;
  }

  @Override
  protected Collection<T> doFetch(Jwt jwt) {
    Map<String, ?> parameter = buildParameter(jwt);
    return provider.fetch(converter.convert(jwt), parameter);
  }

  protected abstract Map<String, ?> buildParameter(Jwt jwt);

  protected List<GrantedAuthority> extractAuthority(Collection<T> providerData) {
    Iterator<T> iterator = providerData.iterator();
    List<GrantedAuthority> permissionsDetail = new ArrayList<>();
    while (iterator.hasNext()) {
      T data = iterator.next();
      List<GrantedAuthority> simpleGrantedAuthorities = data.getPermissions().stream()
          .map(roleName -> "ROLE_" + StringUtils.upperCase(roleName))
          .map(SimpleGrantedAuthority::new)
          .collect(Collectors.toList());
      permissionsDetail.addAll(simpleGrantedAuthorities);
    }
    logger.info("permission list [{}]", permissionsDetail);

    return permissionsDetail;
  }
}
