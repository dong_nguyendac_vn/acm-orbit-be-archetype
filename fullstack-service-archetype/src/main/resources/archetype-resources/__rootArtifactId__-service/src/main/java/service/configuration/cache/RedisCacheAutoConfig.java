#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration.cache;


import ${package}.service.configuration.cache.CacheConfiguration.CacheSpec;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;


@Configuration
@Slf4j
@ConditionalOnProperty(
    name = {"spring.cache.type"},
    havingValue = "redis"
)
@EnableCaching
public class RedisCacheAutoConfig extends AbstractCacheConfig {

  private final RedisConnectionFactory redisConnectionFactory;

  public RedisCacheAutoConfig(
      CacheConfiguration cacheConfiguration,
      RedisConnectionFactory redisConnectionFactory) {
    super(cacheConfiguration);
    this.redisConnectionFactory = redisConnectionFactory;
  }

  @Bean
  public CacheManager cacheManager() {
    Map<String, RedisCacheConfiguration> cacheConfigs = new HashMap<>();
    Map<String, CacheSpec> specs = cacheConfiguration.getSpecs();
    specs.forEach((key, spec) -> {
      log.info("Cache {} with timeout of {} seconds, max cache is depend on redis server",
          key, spec.getTimeout());
      cacheConfigs.put(key, buildRedisCacheConfig(spec));
    });
    return RedisCacheManager.builder(redisConnectionFactory)
        .cacheDefaults(RedisCacheConfiguration.defaultCacheConfig())
        .withInitialCacheConfigurations(cacheConfigs)
        .transactionAware()
        .build();
  }

  private RedisCacheConfiguration buildRedisCacheConfig(CacheSpec cacheSpec) {

    return RedisCacheConfiguration.defaultCacheConfig()
        .entryTtl(Duration.ofSeconds(cacheSpec.getTimeout()))
        .disableCachingNullValues();
  }


}
