#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration;

import ${groupId}.core.IdentifierGenerator;
import ${package}.service.utils.UUIDIdentifierGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IdentifierConfiguration {
  @Bean
  public IdentifierGenerator identifierGenerator(){
    return new UUIDIdentifierGenerator();
  }
}
