#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.service;

import ${package}.service.security.converter.BearerAuthorizationConverter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

@RequiredArgsConstructor
public abstract class AbstractJwtSecurityPermission<T> implements
    JwtSecurityPermissionService {

  protected final Logger logger;
  protected final Converter<Jwt, String> converter = new BearerAuthorizationConverter();

  @Override
  public List<GrantedAuthority> fetch(Jwt jwt) {
    T fetchData = doFetch(jwt);
    return extractAuthority(fetchData);
  }

  protected abstract List<GrantedAuthority> extractAuthority(T providerData);

  protected abstract T doFetch(Jwt jwt);
}
