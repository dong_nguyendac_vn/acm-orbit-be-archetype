#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.valueobject;

import ${groupId}.core.ValueObject;
import ${package}.domain.enums.AdditionalType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Getter
@Builder
@EqualsAndHashCode
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Embeddable
public class AdditionalPk implements ValueObject {

  private static final long serialVersionUID = -8547876560502790630L;
  private final String key;
  private final String referenceId;
  @Enumerated(EnumType.STRING)
  private final AdditionalType type;
}
