#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error;

import java.util.Map;

public interface ErrorHandler {

  Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable);

}
