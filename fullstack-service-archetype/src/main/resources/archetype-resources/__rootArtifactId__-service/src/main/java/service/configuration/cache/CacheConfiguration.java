#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration.cache;

import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "cache")
@Data
public class CacheConfiguration {

  private Map<String, CacheSpec> specs;

  @Data
  static class CacheSpec {

    private Integer timeout;
    private Integer max = 50;
  }


}
