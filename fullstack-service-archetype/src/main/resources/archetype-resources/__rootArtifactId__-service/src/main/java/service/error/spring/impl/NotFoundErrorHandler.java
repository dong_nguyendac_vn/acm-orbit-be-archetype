#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.spring.impl;

import ${groupId}.core.dto.response.ErrorResponse;
import ${groupId}.core.exception.message.NotFoundMessage;
import ${package}.service.error.ErrorHandler;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component("404")
public class NotFoundErrorHandler implements ErrorHandler {

  @Override
  public Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable) {
    return ErrorResponse.builder().error(NotFoundMessage.NOT_FOUND.getMessage())
        .errorDescription(NotFoundMessage.NOT_FOUND.getDescription()).build()
        .toMap();
  }

}
