#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.model;

import ${groupId}.core.IdentifiableDomainObject;
import ${groupId}.core.valueobject.Timestamp;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
@Getter
@Setter
@NoArgsConstructor
public abstract class Auditable<T extends Serializable> implements IdentifiableDomainObject<T> {

  private static final long serialVersionUID = 2014271243101975136L;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "created_at", updatable = false))
  @CreatedDate
  protected Timestamp createdAt;

  @Embedded
  @LastModifiedDate
  @AttributeOverride(name = "value", column = @Column(name = "last_modified_at"))
  protected Timestamp lastModifiedAt;
}
