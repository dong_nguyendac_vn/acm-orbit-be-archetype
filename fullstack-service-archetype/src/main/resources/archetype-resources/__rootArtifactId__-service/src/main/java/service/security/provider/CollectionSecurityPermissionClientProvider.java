#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.provider;

import ${package}.service.security.dto.SecurityPermission;
import java.util.Collection;
import java.util.Map;

public interface CollectionSecurityPermissionClientProvider<T extends SecurityPermission> extends
    SecurityPermissionClientProvider<T> {

  Collection<T> fetch(String token, Map<String, ?> data);
}
