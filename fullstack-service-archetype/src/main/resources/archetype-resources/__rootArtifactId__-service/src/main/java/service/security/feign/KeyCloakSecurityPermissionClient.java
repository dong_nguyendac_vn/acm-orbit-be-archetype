#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.feign;

import ${package}.service.security.dto.KeyCloakSecurityPermission;
import ${package}.service.security.feign.configuration.FormDataConfiguration;
import ${package}.service.security.feign.configuration.LogLevelConfiguration;
import java.util.Collection;
import java.util.Map;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "KeyCloakSecurityPermissionProvider",
    url = "${keycloak.realm-url}", configuration = {FormDataConfiguration.class,
    LogLevelConfiguration.class})
public interface KeyCloakSecurityPermissionClient {

  @PostMapping(value = "/protocol/openid-connect/token",
      consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  Collection<KeyCloakSecurityPermission> fetch(@RequestHeader("Authorization") String token,
      Map<String, ?> parameters);
}
