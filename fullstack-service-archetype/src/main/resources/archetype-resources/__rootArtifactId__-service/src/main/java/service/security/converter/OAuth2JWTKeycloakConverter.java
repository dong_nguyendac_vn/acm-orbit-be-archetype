#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.converter;

import ${package}.service.security.service.JwtSecurityPermissionService;
import java.util.Collection;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;

@RequiredArgsConstructor
public class OAuth2JWTKeycloakConverter extends JwtAuthenticationConverter {

  private final JwtSecurityPermissionService permissionService;

  @Override
  protected Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
    Collection<GrantedAuthority> grantedAuthorities = super.extractAuthorities(jwt);
    grantedAuthorities
        .addAll(permissionService.fetch(jwt));
    return grantedAuthorities;
  }
}
