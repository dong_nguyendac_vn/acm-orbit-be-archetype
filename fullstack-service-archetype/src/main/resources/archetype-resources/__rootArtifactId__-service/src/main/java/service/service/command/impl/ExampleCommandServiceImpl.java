#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.service.command.impl;

import ${package}.domain.commands.ExampleCreateCommand;
import ${package}.service.mapper.example.ExampleCreateMapper;
import ${package}.service.repository.ExampleRepository;
import ${package}.service.service.command.ExampleCommandService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
@Profile("command")
public class ExampleCommandServiceImpl implements ExampleCommandService {

  private final ExampleRepository repository;
  private final ExampleCreateMapper createMapper;

  @Override
  public String create(ExampleCreateCommand command) {
    log.info("create example with identifier {}", command.getIdentifier());
    return  repository.save(createMapper.transform(command.getIdentifier(), command)).getId();
  }
}
