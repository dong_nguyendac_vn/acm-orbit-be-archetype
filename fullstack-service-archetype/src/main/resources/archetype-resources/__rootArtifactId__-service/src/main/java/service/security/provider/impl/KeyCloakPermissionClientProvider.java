#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.provider.impl;

import ${package}.service.security.dto.KeyCloakSecurityPermission;
import ${package}.service.security.feign.KeyCloakSecurityPermissionClient;
import ${package}.service.security.provider.CollectionSecurityPermissionClientProvider;
import java.util.Collection;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class KeyCloakPermissionClientProvider implements
    CollectionSecurityPermissionClientProvider<KeyCloakSecurityPermission> {

  private final KeyCloakSecurityPermissionClient client;

  @Override
  public Collection<KeyCloakSecurityPermission> fetch(String token, Map<String, ?> data) {
    return client.fetch(token, data);
  }
}
