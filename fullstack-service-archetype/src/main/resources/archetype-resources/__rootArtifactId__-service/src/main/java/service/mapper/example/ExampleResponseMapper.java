#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.mapper.example;

import ${groupId}.core.mapper.ResponseMapper;
import ${package}.domain.dto.response.ExampleDTO;
import ${package}.service.model.Example;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ExampleResponseMapper extends ResponseMapper<ExampleDTO, Example> {

  ExampleDTO transform(Example domain);
}