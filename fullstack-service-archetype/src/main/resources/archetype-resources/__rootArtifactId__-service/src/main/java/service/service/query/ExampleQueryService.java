#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.service.query;

import ${package}.domain.dto.response.ExampleDTO;
import java.util.Optional;

public interface ExampleQueryService {

  public Optional<ExampleDTO> get(String id);

}
