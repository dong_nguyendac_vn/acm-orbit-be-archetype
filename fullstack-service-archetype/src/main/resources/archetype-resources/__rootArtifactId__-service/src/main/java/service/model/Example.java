#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.model;

import ${package}.domain.enums.ExampleType;
import ${package}.domain.enums.Status;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Table(name = "example")
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
@Getter
@NoArgsConstructor(force = true)
@EntityListeners(AuditingEntityListener.class)
public class Example extends Auditable<String> {

  private static final long serialVersionUID = 7972589914089140208L;
  @Setter
  @Id
  private String id;

  @Setter
  private String name;

  @Setter
  private String description;

  @Setter
  @Enumerated(EnumType.STRING)
  private ExampleType type;

  @Setter
  @Enumerated(EnumType.STRING)
  private Status status;

}
