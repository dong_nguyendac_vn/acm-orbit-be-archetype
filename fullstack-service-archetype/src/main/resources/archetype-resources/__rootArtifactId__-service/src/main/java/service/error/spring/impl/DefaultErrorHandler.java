#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.spring.impl;

import ${groupId}.core.dto.response.ErrorResponse;
import ${package}.service.error.ErrorHandler;
import ${groupId}.core.exception.message.InternalServerMessage;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component("defaultErrorHandler")
public class DefaultErrorHandler implements ErrorHandler {

  @Override
  public Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable) {
    return ErrorResponse.builder().error(InternalServerMessage.INTERNAL_SERVER_ERROR.getMessage())
        .errorDescription(InternalServerMessage.INTERNAL_SERVER_ERROR.getDescription()).build()
        .toMap();
  }

}
