#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.service.command;

import ${package}.domain.commands.ExampleCreateCommand;
import ${package}.domain.commands.ExampleUpdateCommand;
import java.util.concurrent.CompletableFuture;

public interface ExampleCommandService {

  String create(ExampleCreateCommand command);

}
