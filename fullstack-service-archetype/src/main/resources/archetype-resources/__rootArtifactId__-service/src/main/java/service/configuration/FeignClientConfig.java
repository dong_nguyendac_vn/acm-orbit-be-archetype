#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.partner.service.configuration;

import okhttp3.OkHttpClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"${package}.partner.service"})
public class FeignClientConfig {

  @Bean
  public OkHttpClient.Builder okHttpClientBuilder() {
    return new OkHttpClient.Builder();
  }

}
