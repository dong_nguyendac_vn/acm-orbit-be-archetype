#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.api.external;

import ${groupId}.core.IdentifierGenerator;
import ${groupId}.core.dto.response.IdResponse;
import ${package}.domain.commands.ExampleCreateCommand;
import ${package}.domain.dto.request.ExampleDTO;
import ${package}.service.mapper.example.ExampleCreateMapper;
import ${package}.service.service.command.ExampleCommandService;
import ${package}.service.utils.ResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("example")
@RequiredArgsConstructor
@Slf4j
@Profile("command")
public class ExampleCommandController {

  private final ExampleCommandService service;
  private final ExampleCreateMapper createMapper;
  private final IdentifierGenerator identifierGenerator;

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<IdResponse> create(
      @RequestBody @Validated ExampleDTO request) {
    log.info("create example data");
    ExampleCreateCommand command = createMapper
        .transform(identifierGenerator.generateIdentifier(), request);
    IdResponse idResponse = IdResponse.builder().id(service.create(command)).build();
    return ResponseFactory.accepted(idResponse);
  }
}
