#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration.cache;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class AbstractCacheConfig implements Cacheable {
  protected final CacheConfiguration cacheConfiguration;
}
