#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.spring.impl;

import ${groupId}.core.dto.response.ErrorResponse;
import ${groupId}.core.exception.message.MethodNotAllowedMessage;
import ${package}.service.error.ErrorHandler;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component("405")
public class MethodNotSupportedErrorHandler implements ErrorHandler {

  @Override
  public Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable) {
    return ErrorResponse.builder().error(MethodNotAllowedMessage.METHOD_NOT_ALLOWED.getMessage())
        .errorDescription(MethodNotAllowedMessage.METHOD_NOT_ALLOWED.getDescription()).build()
        .toMap();
  }

}
