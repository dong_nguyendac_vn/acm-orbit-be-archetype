#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.converter;

import javax.annotation.Nonnull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.oauth2.jwt.Jwt;

public class BearerAuthorizationConverter implements Converter<Jwt, String> {

  private static final String BEARER = "Bearer";

  @Override
  public String convert(@Nonnull Jwt token) {
    return String.format("%s %s", BEARER, token.getTokenValue());
  }
}
