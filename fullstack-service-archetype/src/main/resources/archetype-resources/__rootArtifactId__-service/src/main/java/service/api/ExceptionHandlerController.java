#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

package ${package}.service.api;

import ${groupId}.core.exception.ApplicationException;
import ${groupId}.core.exception.message.BadRequestMessage;
import ${package}.service.utils.ResponseFactory;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
@Slf4j
public class ExceptionHandlerController {

  @ExceptionHandler(value = ApplicationException.class)
  public ResponseEntity handleApplicationException(ApplicationException exception) {
    log.error("Application error", exception);
    return ResponseFactory.error(exception.getError());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity handleMethodArgumentNotValidException(
      MethodArgumentNotValidException exception) {
    log.error("Invalid request with error", exception);
    return ResponseFactory.error(BadRequestMessage.INVALID_REQUEST_ERROR);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity handleMethodArgumentTypeMismatchException(
      MethodArgumentTypeMismatchException e) {
    log.error("Invalid request with error", e);
    Class<?> type = e.getRequiredType();
    String message = BadRequestMessage.INVALID_REQUEST_ERROR.getDescription();
    if (Objects.requireNonNull(type).isEnum()) {
      message = "Request field [" + e.getName() + "] must have a value among : [" + String
          .join(",", StringUtils.join(type.getEnumConstants(), ",") + "]");
    }
    return ResponseFactory.error(BadRequestMessage.INVALID_REQUEST_ERROR.getStatusCode(),
        BadRequestMessage.INVALID_REQUEST_ERROR.getMessage(), message);
  }
}
