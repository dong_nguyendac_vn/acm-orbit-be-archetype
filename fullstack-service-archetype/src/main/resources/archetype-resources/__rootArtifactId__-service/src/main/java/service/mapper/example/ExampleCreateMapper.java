#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.mapper.example;


import ${groupId}.core.mapper.CommandMapper;
import ${groupId}.core.mapper.RequestMapper;
import ${package}.domain.commands.ExampleCreateCommand;
import ${package}.domain.dto.request.ExampleDTO;
import ${package}.service.model.Example;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ExampleCreateMapper extends
    RequestMapper<ExampleDTO, ExampleCreateCommand>,
    CommandMapper<Example, ExampleCreateCommand> {

  @Mappings({
      @Mapping(target = "identifier", source = "identifier"),
      @Mapping(target = "name", source = "request.name"),
      @Mapping(target = "type", source = "request.type"),
      @Mapping(target = "description", source = "request.description")
  })
  ExampleCreateCommand transform(String identifier, ExampleDTO request);

}
