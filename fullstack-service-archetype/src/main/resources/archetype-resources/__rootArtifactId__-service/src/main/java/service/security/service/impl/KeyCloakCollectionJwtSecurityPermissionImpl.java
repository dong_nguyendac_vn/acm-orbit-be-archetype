#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.service.impl;

import ${package}.service.security.dto.KeyCloakSecurityPermission;
import ${package}.service.security.provider.CollectionSecurityPermissionClientProvider;
import ${package}.service.security.service.AbstractCollectionJwtSecurityPermission;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KeyCloakCollectionJwtSecurityPermissionImpl extends
    AbstractCollectionJwtSecurityPermission<KeyCloakSecurityPermission> {

  private static final String GRANT_TYPE = "grant_type";
  private static final String AUDIENCE = "audience";
  private static final String RESPONSE_MODE = "response_mode";
  private static final String PERMISSION = "permission";
  private static final String RESOURCES_NAME = "partners";

  public KeyCloakCollectionJwtSecurityPermissionImpl(
      CollectionSecurityPermissionClientProvider<KeyCloakSecurityPermission> provider) {
    super(log, provider);
  }

  @Override
  protected Map<String, Object> buildParameter(Jwt jwt) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put(GRANT_TYPE, "urn:ietf:params:oauth:grant-type:uma-ticket");
    parameters.put(AUDIENCE, jwt.getClaimAsString("azp"));
    parameters.put(RESPONSE_MODE, "permissions");
    parameters.put(PERMISSION, RESOURCES_NAME);
    return parameters;
  }
}
