#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.model;

import ${groupId}.core.IdentifiableDomainObject;
import ${package}.service.valueobject.AdditionalPk;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@EqualsAndHashCode
@Entity
@Table(name = "additional")
@Getter
@NoArgsConstructor(force = true)
@EntityListeners(AuditingEntityListener.class)
public class Additional implements IdentifiableDomainObject<AdditionalPk> {

  private static final long serialVersionUID = 3623645095198248275L;

  @Setter
  @EmbeddedId
  @AttributeOverrides({
      @AttributeOverride(name = "referenceId", column = @Column(name = "reference_id"))
  })
  private AdditionalPk id;

  @Setter
  private String value;

  @Setter
  @Basic(fetch = FetchType.LAZY)
  @ManyToOne
  @JoinColumn(name = "reference_id", updatable = false, insertable = false)
  private Example example;

}
