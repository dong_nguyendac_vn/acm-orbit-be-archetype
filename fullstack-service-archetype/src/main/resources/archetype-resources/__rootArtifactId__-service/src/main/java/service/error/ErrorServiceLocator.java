#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;

public interface ErrorServiceLocator {

  ErrorHandler get(String errorText) throws NoSuchBeanDefinitionException;


}
