#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.service.query.impl;

import ${package}.domain.dto.response.ExampleDTO;
import ${package}.service.mapper.example.ExampleResponseMapper;
import ${package}.service.repository.ExampleRepository;
import ${package}.service.service.query.ExampleQueryService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Profile("query")
public class ExampleQueryServiceImpl implements ExampleQueryService {

  private final ExampleRepository repository;
  private final ExampleResponseMapper mapper;

  @Override
  @Transactional(readOnly = true)
  public Optional<ExampleDTO> get(String id) {
    return repository.findById(id).map(mapper::transform);
  }

}
