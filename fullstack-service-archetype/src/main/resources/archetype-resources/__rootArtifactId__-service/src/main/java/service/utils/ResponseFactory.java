#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.utils;

import ${groupId}.core.dto.response.ErrorResponse;
import ${groupId}.core.exception.message.ApplicationMessage;
import java.io.Serializable;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResponseFactory {

  public static <T extends Serializable> ResponseEntity<T> accepted(@Nullable T data) {
    return ResponseEntity.status(HttpStatus.ACCEPTED).body(data);
  }

  public static <T extends Serializable> ResponseEntity<T> success(@Nullable T data) {
    return ResponseEntity.status(HttpStatus.OK).body(data);
  }

  public static <T extends Serializable> ResponseEntity<T> success() {
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  public static ResponseEntity<ErrorResponse> error(ApplicationMessage error) {
    ErrorResponse errorResponse = ErrorResponse.builder().error(error.getMessage())
        .errorDescription(error.getDescription()).build();
    return ResponseEntity.status(error.getStatusCode()).body(errorResponse);
  }

  public static ResponseEntity<ErrorResponse> error(int status, String error, String description) {
    ErrorResponse errorResponse = ErrorResponse.builder().error(error)
        .errorDescription(description).build();
    return ResponseEntity.status(status).body(errorResponse);
  }
}
