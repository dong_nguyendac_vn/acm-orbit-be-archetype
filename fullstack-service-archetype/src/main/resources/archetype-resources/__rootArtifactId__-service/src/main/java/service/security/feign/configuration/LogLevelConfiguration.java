#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.feign.configuration;

import feign.Logger;
import feign.Logger.Level;
import org.springframework.context.annotation.Bean;

public class LogLevelConfiguration {

  @Bean
  Logger.Level feignLoggerLevel() {
    return Level.FULL;
  }
}
