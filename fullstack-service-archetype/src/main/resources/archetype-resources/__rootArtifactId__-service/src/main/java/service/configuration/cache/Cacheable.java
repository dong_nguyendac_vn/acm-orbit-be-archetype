#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration.cache;

import org.springframework.cache.CacheManager;

public interface Cacheable {
  CacheManager cacheManager();
}
