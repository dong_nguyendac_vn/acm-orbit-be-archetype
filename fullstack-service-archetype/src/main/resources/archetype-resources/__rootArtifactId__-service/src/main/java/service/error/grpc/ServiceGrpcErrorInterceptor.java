#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.grpc;

import ${package}.service.error.ErrorHandler;
import ${package}.service.error.ErrorServiceLocator;
import io.grpc.ForwardingServerCallListener.SimpleForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcGlobalInterceptor;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@GRpcGlobalInterceptor
@Slf4j
@RequiredArgsConstructor
public class ServiceGrpcErrorInterceptor implements ServerInterceptor {

  private final ErrorServiceLocator serviceFactory;

  @Qualifier("defaultGrpcErrorHandler")
  private final ErrorHandler defaultGrpcError;

  @Override
  public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall,
      Metadata metadata,
      ServerCallHandler<ReqT, RespT> serverCallHandler) {
    ServerCall.Listener<ReqT> listener = serverCallHandler.startCall(serverCall, metadata);
    return new ExceptionHandlingServerCallListener<>(listener, serverCall, metadata,
        serviceFactory, defaultGrpcError);
  }

  private static class ExceptionHandlingServerCallListener<ReqT, RespT>
      extends SimpleForwardingServerCallListener<ReqT> {

    private final ServerCall<ReqT, RespT> serverCall;
    private final Metadata metadata;
    private final ErrorServiceLocator serviceFactory;
    private final ErrorHandler defaultGrpcError;

    ExceptionHandlingServerCallListener(Listener<ReqT> delegate,
        ServerCall<ReqT, RespT> serverCall, Metadata metadata,
        ErrorServiceLocator serviceFactory,
        ErrorHandler defaultGrpcError) {
      super(delegate);
      this.serverCall = serverCall;
      this.metadata = metadata;
      this.serviceFactory = serviceFactory;
      this.defaultGrpcError = defaultGrpcError;
    }

    @Override
    public void onHalfClose() {
      try {
        super.onHalfClose();
      } catch (RuntimeException ex) {
        handleException(ex, serverCall, metadata);
        throw ex;
      }
    }

    @Override
    public void onReady() {
      try {
        super.onReady();
      } catch (RuntimeException ex) {
        handleException(ex, serverCall, metadata);
        throw ex;
      }
    }

    private void handleException(RuntimeException exception, ServerCall<ReqT, RespT> serverCall,
        Metadata metadata) {
      log.error("GRPC request got error", exception);
      Map<String, Object> errorMap = this.build(exception);
      serverCall
          .close(Status.INTERNAL.withDescription(errorMap.get("error").toString()), metadata);
    }

    private Map<String, Object> build(Throwable throwable) {
      try {
        return serviceFactory.get(throwable.getClass().getName()).build(null, throwable);
      } catch (NoSuchBeanDefinitionException ex) {
        return defaultGrpcError.build(null, throwable);
      }
    }
  }


}



