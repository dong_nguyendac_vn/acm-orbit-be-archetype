#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.api.external;

import static ${groupId}.core.exception.message.NotFoundMessage.NOT_FOUND;

import ${groupId}.core.exception.ApplicationException;
import ${package}.domain.dto.response.ExampleDTO;
import ${package}.service.service.query.ExampleQueryService;
import ${package}.service.utils.ResponseFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("example")
@RequiredArgsConstructor
@Validated
@Profile("query")
public class ExampleQueryController {

  private final ExampleQueryService service;

  @GetMapping(value = "{example_id}",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ExampleDTO> get(@PathVariable("example_id") String id) {
    return service.get(id)
        .map(ResponseFactory::success)
        .orElseThrow(() -> new ApplicationException(
            NOT_FOUND));
  }
}
