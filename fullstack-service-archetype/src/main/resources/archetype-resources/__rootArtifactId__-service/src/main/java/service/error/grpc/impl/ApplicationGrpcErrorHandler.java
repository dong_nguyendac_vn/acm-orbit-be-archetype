#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.error.grpc.impl;

import ${groupId}.core.dto.response.ErrorResponse;
import ${groupId}.core.exception.ApplicationException;
import ${groupId}.core.exception.message.InternalServerMessage;
import ${package}.service.error.ErrorHandler;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component("ApplicationException")
public class ApplicationGrpcErrorHandler implements ErrorHandler {

  @Override
  public Map<String, Object> build(Map<String, Object> errorAttributes, Throwable throwable) {
    if (throwable instanceof ApplicationException) {
      ApplicationException applicationException = (ApplicationException) throwable;
      return ErrorResponse.builder().error(applicationException.getError().getMessage())
          .errorDescription(applicationException.getError().getDescription()).build()
          .toMap();
    }
    return ErrorResponse.builder().error(InternalServerMessage.INTERNAL_SERVER_ERROR.getKey())
        .errorDescription(InternalServerMessage.INTERNAL_SERVER_ERROR.getDescription()).build()
        .toMap();
  }

}
