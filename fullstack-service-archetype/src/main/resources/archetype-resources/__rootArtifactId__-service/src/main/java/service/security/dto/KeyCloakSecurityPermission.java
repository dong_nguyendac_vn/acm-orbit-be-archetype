#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor(force = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class KeyCloakSecurityPermission implements SecurityPermission {

  private static final long serialVersionUID = 8033963697128241020L;

  private final List<String> scopes;

  @JsonProperty("rsid")
  private final String resourceId;

  @JsonProperty("rsname")
  private final String resourceName;


  @Override
  public List<String> getPermissions() {
    return this.getScopes();
  }
}
