#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.utils;

import ${groupId}.core.IdentifierGenerator;
import java.util.UUID;

public class UUIDIdentifierGenerator implements IdentifierGenerator {

  @Override
  public String generateIdentifier() {
    return UUID.randomUUID().toString();
  }
}
