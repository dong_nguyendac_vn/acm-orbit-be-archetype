#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.feign.configuration;

import feign.codec.Encoder;
import feign.form.FormEncoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;

public class FormDataConfiguration {

  private ObjectFactory<HttpMessageConverters> messageConverters;

  public FormDataConfiguration(ObjectFactory<HttpMessageConverters> messageConverters) {
    this.messageConverters = messageConverters;
  }

  @Bean
  public Encoder feignFormEncoder() {
    return new FormEncoder(new SpringEncoder(messageConverters));
  }
}
