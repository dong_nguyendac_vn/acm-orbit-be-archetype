#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.security.dto;

import com.ascendcorp.orbit.core.ValueObject;
import java.util.List;

public interface SecurityPermission extends ValueObject {

  List<String> getPermissions();
}
