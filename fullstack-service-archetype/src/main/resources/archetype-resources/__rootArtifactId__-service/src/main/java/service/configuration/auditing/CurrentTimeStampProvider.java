#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration.auditing;

import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component("currentTimeStampProvider")
public class CurrentTimeStampProvider implements DateTimeProvider {

  @Override
  @NonNull
  public Optional<TemporalAccessor> getNow() {
    return Optional.of(Instant.now());
  }
}
