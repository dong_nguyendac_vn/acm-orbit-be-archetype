#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration;

import static capital.scalable.restdocs.SnippetRegistry.AUTO_REQUEST_FIELDS;

import capital.scalable.restdocs.payload.AbstractJacksonFieldSnippet;
import capital.scalable.restdocs.payload.JacksonRequestFieldSnippet;
import com.ascendcorp.orbit.core.dto.request.PagingRequestDTO;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.method.HandlerMethod;

public class ACMJacksonRequestFieldSnippet extends AbstractJacksonFieldSnippet {

  private final Type requestBodyType;
  private final boolean failOnUndocumentedFields;

  public ACMJacksonRequestFieldSnippet() {
    this(null, false);
  }

  public ACMJacksonRequestFieldSnippet(Type requestBodyType, boolean failOnUndocumentedFields) {
    super(AUTO_REQUEST_FIELDS, null);
    this.requestBodyType = requestBodyType;
    this.failOnUndocumentedFields = failOnUndocumentedFields;
  }

  public JacksonRequestFieldSnippet requestBodyAsType(Type requestBodyType) {
    return new JacksonRequestFieldSnippet(requestBodyType, failOnUndocumentedFields);
  }

  public JacksonRequestFieldSnippet failOnUndocumentedFields(boolean failOnUndocumentedFields) {
    return new JacksonRequestFieldSnippet(requestBodyType, failOnUndocumentedFields);
  }

  @Override
  protected Type getType(HandlerMethod method) {
    if (requestBodyType != null) {
      return requestBodyType;
    }
    for (MethodParameter param : method.getMethodParameters()) {
      if (isRequestBody(param) || isModelAttribute(param)) {
        return getType(param);
      }
    }
    return null;
  }

  private boolean isPagingObject(MethodParameter param) {
    return PagingRequestDTO.class.isAssignableFrom(param.getParameterType());
  }

  private boolean isRequestBody(MethodParameter param) {
    return param.getParameterAnnotation(RequestBody.class) != null;
  }

  private boolean isModelAttribute(MethodParameter param) {
    return param.getParameterAnnotation(ModelAttribute.class) != null;
  }

  private Type getType(final MethodParameter param) {
    if (isPagingObject(param)) {
      return param.getParameter().getParameterizedType();
    }
    if (isCollection(param.getParameterType())) {
      return (GenericArrayType) () -> firstGenericType(param);
    }
    return param.getParameterType();


  }

  @Override
  public String getHeaderKey() {
    return "request-fields";
  }

  @Override
  protected boolean shouldFailOnUndocumentedFields() {
    return failOnUndocumentedFields;
  }

  @Override
  protected String[] getTranslationKeys() {
    return new String[]{
        "th-path",
        "th-type",
        "th-optional",
        "th-description",
        "no-request-body"
    };
  }
}
