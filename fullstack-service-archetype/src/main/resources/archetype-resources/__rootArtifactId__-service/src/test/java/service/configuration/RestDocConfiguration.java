#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration;

import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.autoconfigure.restdocs.RestDocsMockMvcConfigurationCustomizer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.templates.TemplateFormats;

@TestConfiguration
@RequiredArgsConstructor
public class RestDocConfiguration implements RestDocsMockMvcConfigurationCustomizer {

  private final ObjectMapper objectMapper;

  @Override
  public void customize(MockMvcRestDocumentationConfigurer configurer) {
    configurer.operationPreprocessors()
        .withResponseDefaults(ResponseModifyingPreprocessors.replaceBinaryContent(),
            ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper), prettyPrint()).and()
        .snippets()
        .withDefaults(CliDocumentation.curlRequest(), HttpDocumentation.httpRequest(),
            HttpDocumentation.httpResponse(), AutoDocumentation.responseFields(),
            new ACMJacksonRequestFieldSnippet(), AutoDocumentation.pathParameters(),
            AutoDocumentation.description(), AutoDocumentation.methodAndPath(),
            AutoDocumentation.section(), AutoDocumentation.requestParameters(),
            AutoDocumentation.requestHeaders())
        .and().snippets()
        .withTemplateFormat(TemplateFormats.asciidoctor());
  }

  @Bean
  public RestDocumentationResultHandler restDocumentation() {
    return MockMvcRestDocumentation.document("{ClassName}/{methodName}");
  }

}
