#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.configuration;

import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcBuilderCustomizer;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder;

@TestConfiguration
@RequiredArgsConstructor
public class WebMockMvcBuilderCustomizer implements MockMvcBuilderCustomizer {

  private final RestDocumentationResultHandler restDocumentationResultHandler;
  private final ObjectMapper objectMapper;

  @Override
  public void customize(ConfigurableMockMvcBuilder<?> builder) {
    builder
        .alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
        .alwaysDo(restDocumentationResultHandler);
  }
}
