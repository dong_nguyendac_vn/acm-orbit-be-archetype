#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service.api.external;


import static org.assertj.core.api.Assertions.assertThat;

import com.ascendcorp.orbit.core.IdentifierGenerator;
import ${package}.service.configuration.RestDocConfiguration;
import ${package}.service.configuration.TestSecurityConfiguration;
import ${package}.service.configuration.WebMockMvcBuilderCustomizer;
import ${package}.service.configuration.error.ErrorServiceLocatorConfig;
import ${package}.service.error.spring.impl.DefaultErrorHandler;
import ${package}.service.mapper.example.ExampleCreateMapper;
import ${package}.service.service.command.ExampleCommandService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.context.TestPropertySource;

@Import({
    DefaultErrorHandler.class,
    ErrorServiceLocatorConfig.class,
    WebMockMvcBuilderCustomizer.class,
    RestDocConfiguration.class,
    TestSecurityConfiguration.class
})
@WebMvcTest(controllers = {ExampleCommandController.class}, excludeFilters = {
    @Filter(classes = EnableWebSecurity.class)})
@AutoConfigureRestDocs
@TestPropertySource(properties = "spring.cloud.config.enabled=false")
public class ExampleCommandControllerTest {

  @MockBean
  private ExampleCommandService service;

  @MockBean
  private ExampleCreateMapper mapper;

  @MockBean
  private IdentifierGenerator generator;

  @Test
  public void testController() {
    assertThat(1).isEqualTo(1);
  }

}